var path = require('path');

module.exports = {
    getPath: function() {
        return path.resolve(__dirname + '/../boards');
    }
};
