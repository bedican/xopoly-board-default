# xopoly default board

This project is the default board for [xopoly](https://www.npmjs.com/package/xopoly).

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
